/*
 * usuario.cpp
 *
 *  Created on: 16 de out de 2019
 *      Author: vitor
 */

#define TAM_LINHA 100

#include <cstring>
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include "usuario.h"

namespace nrc {

	Usuario::Usuario(const char* nome,unsigned int id,const char* time)
	{
		id_usuario = id;
		strncpy(nome_usuario,nome,TAM_NOME - 1);
		strncpy(time_usuario,time,TAM_TIME - 1);
	};

	Usuario::~Usuario()
	{
		std::cout << "Destruindo o usuario (" << id_usuario;
		std::cout << " - " << nome_usuario << ")" << std::endl;
	}

	void Usuario::imprime_old()
	{
		 printf("-------------------------------\n");
		 printf("Usuário: %d\n%s\n%s\n",id_usuario,nome_usuario,time_usuario);
		 printf("-------------------------------\n");
	}

	std::ostream& Usuario::imprime(std::ostream& out) const
		{
			 out << "-------------------------------\n";
			 out << "Usuário:"<< id_usuario << std::endl << nome_usuario << std::endl << time_usuario << std::endl;
			 out << "-------------------------------\n";
			 return out;
		}

	void Usuario::copia(const Usuario* from) //modificado
	 {

	 	strncpy(time_usuario,from->time_usuario,TAM_TIME - 1);
	 	id_usuario = from->id_usuario;
	 	strncpy(nome_usuario,from->nome_usuario,TAM_NOME - 1);

	 }

	 unsigned Usuario::muda_id()
	 {
		 int id = id_usuario;

		 return id;
	 }

	 int Usuario::le_usuario(std::istream& arquivo)
	 {
	 	char linha[TAM_LINHA];

	 	if(!arquivo.getline(linha,TAM_LINHA))
	 	{
	 		return 0;
	 	}

	 	linha[strcspn(linha,"\n")] = 0;

	 	char* param = strtok(linha,",");

	 	if (param == NULL)
	 	{
	 		return 0;
	 	}

	 	int id = strtol(param, 0, 0);
	 	if(id <=0)
	 	{
	 		return 0;
	 	}
	 	char* nome = strtok(NULL,",");
	 	char* time = strtok(NULL,",");

		id_usuario = id;
		strncpy(nome_usuario,nome,TAM_NOME - 1);
		strncpy(time_usuario,time,TAM_TIME - 1);

	 	return 1;
	 }

	const char* Usuario::get_nome() const
	{
		return nome_usuario;
	}
	const char* Usuario::get_time() const
	{
		return time_usuario;
	}
	unsigned int  Usuario::get_id() const
	{
		return id_usuario;
	}

} //namespace nrc



