/*
 * EditarUsuario.h
 *
 *  Created on: 30 de out de 2019
 *      Author: vitor
 */

#ifndef SRC_EDITARUSUARIO_H_
#define SRC_EDITARUSUARIO_H_

#include "menu_opcao.h"

namespace nrc {

class EditarUsuario: public menu_opcao
{
public:
	EditarUsuario(ListaUser& lista):
	menu_opcao("Editar Usuario",lista)
	{};

	int exec();
};

} //namespace nrc



#endif /* SRC_EDITARUSUARIO_H_ */
