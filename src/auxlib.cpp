/*
 * auxlib.cpp
 *
 *  Created on: 29 de out de 2019
 *      Author: vitor
 */

#include "auxlib.h"
#include <string.h>

#define TAM_NUM 50

char* le_linha(char* linha,size_t TAM)
{
	linha[TAM-2] = '\0';
	fgets(linha,TAM,stdin);
	if((linha[TAM-2] != '\0')&&(linha[TAM-2] != '\n'))
	{
		int c;
		while((c = getchar()) != '\n' && (c != '\n'))
		{

		}
	}
	linha[strcspn(linha,"\n")] = 0;
	return linha;

}

int le_numero()
{
	char entrada[TAM_NUM];
	char* fim;
	le_linha(entrada,TAM_NUM);
	int num = strtol(entrada,&fim,0); //converte entrada em int e e aloca um vetor 'fim' para a ultima posição do vetor
	if(*fim != '\0')
	{
		num = -1;
	}
	else if(num == 0)
	{
		entrada[1] = '\0';
		if(strcmp(entrada,"0")) //compara o vetor entrada com "0"
		{
			num = -1;
		}
	}
	return num;
}



