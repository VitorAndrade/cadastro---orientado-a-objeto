/*
 * Menu.h
 *
 *  Created on: 25 de out de 2019
 *      Author: vitor
 */

#ifndef SRC_MENU_H_
#define SRC_MENU_H_

#include <vector>
#include <iostream>
#include <string.h>

//#include "Lista.h"
#include "auxlib.h"
#include "lista_template.h"
#include "lista_usuario.h"
#include "ListarUsuarios.h"
#include "AdicionarUsuarios.h"
#include "RemoverUsuarios.h"
#include "ProcurarId.h"
#include "RemoverId.h"
#include "AdicionarArquivo.h"
#include "SalvarLista.h"


namespace nrc {

class Menu

{
private:
	std::vector <menu_opcao*> lista_opcoes;

	ListaUser& lista_user;

public:
	Menu(ListaUser& lista):
		lista_user(lista)
	{};

	~Menu();
	void adicionar_opcao(menu_opcao* opcao);
	void run();


};//Menu

}//namespace nrc


#endif /* SRC_MENU_H_ */
