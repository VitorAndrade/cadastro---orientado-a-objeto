/*
 * Lista.cpp
 *
 *  Created on: 23 de out de 2019
 *      Author: vitor
 */
#include "Lista.h"
#include <fstream>

namespace nrc {

Lista::Lista()://unsigned int tam,unsigned int total, unsigned int users):
tamanho_lista(0),
total_lista(1),
vetor_usuarios(0)

{
	vetor_usuarios = new Usuario*[total_lista];
};

void Lista::add_usuario(const Usuario* u)
{

	if(getById(u->get_id()) != NULL)
	{
		std::cout << "id já existe";
		return;
	}

	if(tamanho_lista == total_lista)
	{
		total_lista += 5;

		Usuario** novo = new Usuario*[total_lista];

		memcpy(novo,vetor_usuarios,total_lista*sizeof(Usuario*));

		delete [] vetor_usuarios;

		vetor_usuarios = novo;

	}
	Usuario* u2 = new Usuario(u->get_nome(),u->get_id(),u->get_time());
	tamanho_lista = tamanho_lista + 1;
	vetor_usuarios[tamanho_lista - 1] = u2;
}

const unsigned int Lista::get_tamanho_lista() const
{
	return tamanho_lista;
}

/*void Lista::imprime()
{

	for(unsigned int i = 0;i<=tamanho_lista-1;i++)
	{
		vetor_usuarios[i]->imprime(std::cout);
	}

}*/

std::ostream& Lista::imprime_new(std::ostream& out)
{

	for(unsigned int i = 0;i<tamanho_lista;i++)
	{
		out << *vetor_usuarios[i] << "\n";
	}
	return out;
}

Usuario* Lista::getById(unsigned int id)
{

	for(unsigned i = 0; i < tamanho_lista;++i)
	{
		if(vetor_usuarios[i]->get_id() == id)
		{
			//vetor_usuarios[i]->imprime(std::cout);
			return vetor_usuarios[i];
		}

	}
	//printf("id não registrada!\n");

	return NULL;
}

void Lista::remove_id(unsigned id)
{
 	for(unsigned i = 0; i < tamanho_lista;++i)
 	{
 		if(vetor_usuarios[i]->get_id() == id)
 		{
 			delete vetor_usuarios[i];
 			vetor_usuarios[i] = vetor_usuarios[tamanho_lista - 1];
 			vetor_usuarios[tamanho_lista - 1] = NULL;
 			tamanho_lista--;
 			return;
 		}
 	}
 	printf("id não registrada!\n");
}

unsigned int Lista::get_tamanho_lista()
{
	return tamanho_lista;
}

unsigned int Lista::get_total_lista()
{
	return total_lista;
}

Usuario** Lista::get_vetor_usuarios()
{
	return vetor_usuarios;
}

void Lista::add_usuario_dados(unsigned int id,const char* nome,const char* time)
{
	Usuario u(nome,id,time);
	add_usuario(&u);
}

void Lista::limparLista()
{
	for(unsigned int i = 0;i<tamanho_lista;i++)
	{
		delete vetor_usuarios[i];
	}
	delete [] vetor_usuarios;
	tamanho_lista = 0;
	total_lista = 1;
	vetor_usuarios = new Usuario*[total_lista];
}

int Lista::le_lista(const char* nome)
{
	std::ifstream arquivo;
	arquivo.open(nome);
	if (!arquivo)
	{
		return 0;
	}

	limparLista();

	Usuario u;

	for(int flag = u.le_usuario(arquivo); flag != 0;flag = u.le_usuario(arquivo))
	{
		add_usuario(&u);
	}
	return 1;
}

Lista::~Lista()
{
	for(unsigned int i = 0;i < tamanho_lista;i++)
	{
		delete vetor_usuarios[i];
	}
	delete [] vetor_usuarios;

}

} //namespace nrc

