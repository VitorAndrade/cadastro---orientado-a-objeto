/*
 * lista_template.h
 *
 *  Created on: 10 de nov de 2019
 *      Author: vitor
 */

#ifndef SRC_LISTA_TEMPLATE_H_
#define SRC_LISTA_TEMPLATE_H_

#include <iostream>
#include <cstring>
#include <fstream>
#include "usuario.h"
#include "auxlib.h"

namespace nrc {

template <class T>
class List {
		unsigned tamanho;
		unsigned total;
		T** vetor_elementos;
public:
	List():
		tamanho(0),
		total(1),
		vetor_elementos(0)
	{
	vetor_elementos = new T*[total];
	};

	void adicionarElemento(T* elem)
	{
		if(tamanho == total)
		{
			total += 5;

			T** novo = new T*[total];

			memcpy(novo,vetor_elementos,total*sizeof(T*));

			delete [] vetor_elementos;

			vetor_elementos = novo;

		}
		T* elem2 = new T(*elem);

		tamanho = tamanho + 1;
		vetor_elementos[tamanho - 1] = elem2;
	}

	std::ostream& imprime_new(std::ostream& out)
	{

		for(unsigned int i = 0;i<tamanho;i++)
		{
			out << *vetor_elementos[i] << "\n";
		}
		return out;
	}

	void remove_elemento(T* elem)
	{
	 	for(unsigned i = 0; i < tamanho;++i)
	 	{
	 		if(vetor_elementos[i] == elem)
	 		{
	 			delete vetor_elementos[i];
	 			vetor_elementos[i] = vetor_elementos[tamanho - 1];
	 			vetor_elementos[tamanho - 1] = NULL;
	 			tamanho--;
	 			return;
	 		}
	 	}
	 	printf("elemento não registrado!\n");
	}

	void limparLista()
	{
		for(unsigned int i = 0;i<tamanho;i++)
		{
			delete vetor_elementos[i];
		}
		delete [] vetor_elementos;
		tamanho = 0;
		total = 1;
		vetor_elementos= new T*[total];
	}

	void addElementoDados(T* elem)
	{
		adicionarElemento(elem);
	}

	unsigned int get_tamanho() const
	{
		return tamanho;
	}

	unsigned int get_total() const
	{
		return total;
	}

	T* get_elemento(unsigned e)
	{
		return vetor_elementos[e];
	}


};
/*
template <>
class List <Usuario> {
		unsigned tamanho;
		unsigned total;
		Usuario** vetor_elementos;
public:
	List():
		tamanho(0),
		total(1),
		vetor_elementos(0)
	{
	vetor_elementos = new Usuario*[total];
	};

	void adicionarElemento(Usuario* elem)
	{

		if(getById(elem->get_id()) != NULL)
		{
			std::cout << "id já existe";
			return;
		}

		if(tamanho == total)
			{
				total += 5;

				Usuario** novo = new Usuario*[total];

				memcpy(novo,vetor_elementos,total*sizeof(Usuario*));

				delete [] vetor_elementos;

				vetor_elementos = novo;
			}
			Usuario* u2 = new Usuario(elem->get_nome(),elem->get_id(),elem->get_time());
			tamanho = tamanho + 1;
			vetor_elementos[tamanho - 1] = u2;
	}

	Usuario* getById(unsigned int id)
	{

		for(unsigned i = 0; i < tamanho;++i)
		{
			if(vetor_elementos[i]->get_id() == id)
			{
				//vetor_usuarios[i]->imprime(std::cout);
				return vetor_elementos[i];
			}

		}
		//printf("id não registrada!\n");

		return NULL;
	}

	std::ostream& imprime_new(std::ostream& out)
	{

		for(unsigned int i = 0;i<tamanho;i++)
		{
			out << *vetor_elementos[i] << "\n";
		}
		return out;
	}

	void remove_id(unsigned id)
	{
	 	for(unsigned i = 0; i < tamanho;++i)
	 	{
	 		if(vetor_elementos[i]->get_id() == id)
	 		{
	 			delete vetor_elementos[i];
	 			vetor_elementos[i] = vetor_elementos[tamanho - 1];
	 			vetor_elementos[tamanho - 1] = NULL;
	 			tamanho--;
	 			return;
	 		}
	 	}
	 	printf("id não registrada!\n");
	}


};*/

}//namespace nrc




#endif /* SRC_LISTA_TEMPLATE_H_ */
