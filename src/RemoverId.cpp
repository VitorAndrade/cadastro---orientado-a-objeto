/*
 * RemoverId.cpp
 *
 *  Created on: 30 de out de 2019
 *      Author: vitor
 */

#include "RemoverId.h"

namespace nrc {

int RemoverId::exec()
{
	int id = 0;

	for(;;)
	{
		printf("digite a id:\n");
		id = le_numero();
		if(id == -1)
		{
			printf("valor inválido, tente novamente\n");
			continue;
		}
		if(!_lista.getById(id))
		{
			printf("id não cadastrada\n");
			return 0;
		}
		break;
	}
	_lista.remove_id(id);

	return 0;

}
}// namespace nrc


