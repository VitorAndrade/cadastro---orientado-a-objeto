/*
 * RemoverUsuarios.h
 *
 *  Created on: 29 de out de 2019
 *      Author: vitor
 */

#ifndef SRC_REMOVERUSUARIOS_H_
#define SRC_REMOVERUSUARIOS_H_

#include "menu_opcao.h"
#include "auxlib.h"


namespace nrc {

class RemoverUsuarios: public menu_opcao
{
public:
	RemoverUsuarios(ListaUser& lista):
		menu_opcao("Remover Usuário",lista)
		{};
	int exec();
};

}//namespace nrc








#endif /* SRC_REMOVERUSUARIOS_H_ */
