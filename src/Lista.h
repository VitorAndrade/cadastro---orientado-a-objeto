/*
 * Lista.h
 *
 *  Created on: 23 de out de 2019
 *      Author: vitor
 */

#ifndef CADASTRO_CPP_CADASTRO_CPP_SRC_LISTA_H_
#define CADASTRO_CPP_CADASTRO_CPP_SRC_LISTA_H_

#define TAM_LISTA 100
#include <cstring>
#include <iostream>

#include "usuario.h"

namespace nrc {

class Lista
{
private:

    unsigned tamanho_lista;
    unsigned total_lista;
    Usuario **vetor_usuarios;

public:
    Lista();
    ~Lista();

    void add_usuario(const Usuario* u);
    const unsigned int get_tamanho_lista() const;
    void imprime();
    std::ostream& imprime_new(std::ostream& out);
    void remove_id(unsigned id);
    unsigned int get_tamanho_lista();
    unsigned int get_total_lista();
    Usuario** get_vetor_usuarios();
    void add_usuario_dados(unsigned int id,const char* nome,const char* time);
    nrc::Usuario* getById(unsigned int id);
    int le_lista(const char* nome);
    void limparLista();

};

} //namespace nrc



#endif /* CADASTRO_CPP_CADASTRO_CPP_SRC_LISTA_H_ */
