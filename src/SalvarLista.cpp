/*
 * SalvarLista.cpp
 *
 *  Created on: 4 de nov de 2019
 *      Author: vitor
 */

#include "SalvarLista.h"
#include "auxlib.h"
#include <fstream>


namespace nrc {

int SalvarLista::exec()
{
	char nome_arquivo[TAM_NOME];
	for(;;)
	{
		printf("digite o nome do arquivo:\n");
		if(le_linha(nome_arquivo,TAM_NOME))
		{
			break;

		}
		printf("Nome inválido!\n");
	}

	std::ofstream arquivo;
	arquivo.open(nome_arquivo);

	for(unsigned int i = 0;i < _lista.get_tamanho();++i)
	{
		Usuario* v = _lista.get_elemento(i);
		arquivo << v[i].get_id() << "," << v[i].get_nome() << "," << v[i].get_time() << "\n";
	}
	arquivo.close();



	return 0;
}

} //namespace nrc
