/*
 * lista_usuario.h
 *
 *  Created on: 11 de nov de 2019
 *      Author: vitor
 */

#ifndef SRC_LISTA_USUARIO_H_
#define SRC_LISTA_USUARIO_H_

#include "lista_template.h"

namespace nrc {


class ListaUser: public List<Usuario>
{

public:
	void adicionarElemento(Usuario* elem)
		{
			if(getById(elem->get_id()) != NULL)
			{
				std::cout << "id já existe";
				return;
			}
			List<Usuario>::adicionarElemento(elem);
		};

	Usuario* getById(unsigned int id)
	{

		for(unsigned i = 0; i < get_tamanho();++i)
		{
			if(List<Usuario>::get_elemento(i)->get_id() == id)
			{
				//vetor_usuarios[i]->imprime(std::cout);
				return get_elemento(i);
			}
		}
		return NULL;
	};

	void imprime(std::ostream& out)
	{
		imprime_new(out);
	};

	void remove_id(unsigned int id)
	{
	 	for(unsigned i = 0; i < get_tamanho();++i)
	 	{
	 		Usuario *u = get_elemento(i);
	 		if(u->get_id() == id)
	 		{
	 			remove_elemento(u);
	 			return;
	 		}
	 	}
	 	printf("elemento não registrado!\n");
	};

	void limparLista()
	{
		List<Usuario>::limparLista();
	}

	int le_lista(const char* nome)
	{
		std::ifstream arquivo;
		arquivo.open(nome);
		if (!arquivo)
		{
			return 0;
		}

		limparLista();

		Usuario u;

		for(int flag = u.le_usuario(arquivo); flag != 0;flag = u.le_usuario(arquivo))
		{
			adicionarElemento(&u);
		}
		return 1;
	}

	void adicionarElementoDados(unsigned int id,const char* nome,const char* time)
	{
		Usuario u(nome,id,time);
		adicionarElemento(&u);
	}

};

}//namespace nrc

#endif /* SRC_LISTA_USUARIO_H_ */
