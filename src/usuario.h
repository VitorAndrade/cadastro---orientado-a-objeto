/*
 * usuario.h
 *
 *  Created on: 16 de out de 2019
 *      Author: vitor
 */

#ifndef USUARIO_H_
#define USUARIO_H_

#include <iostream>

#define TAM_NOME 100
#define TAM_TIME 100
#define TAM_NUM 50

namespace nrc {

class Usuario
{
private:
	char nome_usuario[TAM_NOME];
	unsigned int id_usuario;
	char time_usuario[TAM_TIME];

public:
	Usuario(const char* nome,unsigned int id,const char* time);
	Usuario(): id_usuario(0) {};
	~Usuario();

	void imprime_old();
	std::ostream& imprime(std::ostream& out) const;
	void copia(const Usuario* from); //modificado
	unsigned muda_id();
	int le_usuario(std::istream& arquivo);
	const char* get_nome() const;
	const char* get_time() const;
	unsigned int get_id() const;

};

	inline std::ostream& operator << (std::ostream& out,Usuario& u)
	{
		return u.imprime(out);
	}

}// namespace nrc

#endif /* USUARIO_H_ */
