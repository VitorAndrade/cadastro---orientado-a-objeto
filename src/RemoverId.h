/*
 * RemoverId.h
 *
 *  Created on: 30 de out de 2019
 *      Author: vitor
 */

#ifndef SRC_REMOVERID_H_
#define SRC_REMOVERID_H_

#include "menu_opcao.h"
#include "auxlib.h"

namespace nrc {

class RemoverId: public menu_opcao
{
public:

	RemoverId(ListaUser& lista):
	menu_opcao("Remover por ID",lista)
	{};

	int exec();
};

} //namespace nrc



#endif /* SRC_REMOVERID_H_ */
