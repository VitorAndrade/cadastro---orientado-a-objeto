/*
 * Menu.cpp
 *
 *  Created on: 25 de out de 2019
 *      Author: vitor
 */

#include "Menu.h"

namespace nrc {

void Menu::adicionar_opcao(menu_opcao* opcao)
{
	lista_opcoes.push_back(opcao);
}

void Menu::run()
{

	while(1)
	{
		for(size_t i = 0;i < lista_opcoes.size(); i++)
		{
			std::cout << i + 1 << "    " << lista_opcoes[i]->get_nome() << std::endl;
		}
		std::cout << "0" << "    " << "Sair" << std::endl;

		unsigned int opcao = le_numero();

		if((opcao >= 1) && (opcao <= lista_opcoes.size()))
		{
			(lista_opcoes[opcao - 1]->exec());

		}
		else if(opcao == 0)
		{
			return;
		}
		else
		{
			std::cout << "Opcao Inválida!!" << std::endl;
		}

	}

} //menu

Menu::~Menu()
{
	for(size_t i = 0; i < lista_opcoes.size(); i++)
	{
		delete lista_opcoes[i];
		std::cout << "Deletando Opções do Menu" << std::endl;
	}
};

} //namespace nrc





