/*
 * ListarUsuarios.cpp
 *
 *  Created on: 25 de out de 2019
 *      Author: vitor
 */

#include "ListarUsuarios.h"

namespace nrc {

int ListarUsuarios::exec()
{
	std::cout << "Exibir Lista:\n" << std::endl;
	_lista.imprime_new(std::cout);


	return 0;
}

} //namespace nrc
