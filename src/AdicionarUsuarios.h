/*
 * AdicionarUsuarios.h
 *
 *  Created on: 29 de out de 2019
 *      Author: vitor
 */

#ifndef SRC_ADICIONARUSUARIOS_H_
#define SRC_ADICIONARUSUARIOS_H_

#include "menu_opcao.h"
#include "lista_usuario.h"

namespace nrc {

class AdicionarUsuarios: public menu_opcao
{
public:
	AdicionarUsuarios(ListaUser& lista):
	menu_opcao("Adicionar Usuario",lista)
	{};

	int exec();
};

} //namespace nrc



#endif /* SRC_ADICIONARUSUARIOS_H_ */
