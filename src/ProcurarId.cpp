/*
 * ProcurarId.cpp
 *
 *  Created on: 30 de out de 2019
 *      Author: vitor
 */

#include "ProcurarId.h"
#include "auxlib.h"

int nrc::ProcurarId::exec()
{
	int id = 0;
	Usuario* u;
	std::cout << "digite a id desejada:\n";
	id = le_numero();
	u = _lista.getById(id);

	if(u == NULL)
	{
		std::cout << "id inválida" << std::endl;
		return 0;
	}

	std::cout << *u;


	return 0;
}


