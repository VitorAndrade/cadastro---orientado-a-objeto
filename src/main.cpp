#include <cstring>
#include <cstdio>
#include <cstdlib>

#include "usuario.h"
#include "Lista.h"
#include "Menu.h"
#include "menu_opcao.h"
#include "lista_template.h"
#include "lista_usuario.h"

using namespace nrc;


void teste()
{
	Usuario u1("Geraldo",1,"Barcelona");
	Usuario u2("Vaca",12,"Tabajara");
	//char c = 'a';
	//char d = 'b';
	ListaUser lista_user;
	//List <char> lista_int;

	lista_user.adicionarElemento(&u1);
	lista_user.adicionarElemento(&u2);
	//lista_int.adicionarElemento(&c);
	//lista_int.adicionarElemento(&d);

	unsigned int idd = 12;

	lista_user.imprime_new(std::cout);
	lista_user.remove_id(idd);
	lista_user.imprime_new(std::cout);

	//lista_int.imprime_new(std::cout);
	//lista_int.remove_elemento(&c);
	//lista_int.imprime_new(std::cout);
}



int main()
{

	//teste();
	ListaUser lista_user;


	Menu menu(lista_user);

	menu.adicionar_opcao(new ListarUsuarios(lista_user));
	menu.adicionar_opcao(new AdicionarUsuarios(lista_user));
	menu.adicionar_opcao(new RemoverUsuarios(lista_user));
	menu.adicionar_opcao(new ProcurarId(lista_user));
	menu.adicionar_opcao(new RemoverId(lista_user));
	menu.adicionar_opcao(new AdicionarArquivo(lista_user));
	menu.adicionar_opcao(new SalvarLista(lista_user));



	menu.run();

	return 0;
}


