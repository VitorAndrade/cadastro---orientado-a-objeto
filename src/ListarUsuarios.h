/*
 * ListarUsuarios.h
 *
 *  Created on: 25 de out de 2019
 *      Author: vitor
 */

#ifndef SRC_LISTARUSUARIOS_H_
#define SRC_LISTARUSUARIOS_H_

#include "menu_opcao.h"
#include <iostream>

namespace nrc {

class ListarUsuarios: public menu_opcao
{

public:
	ListarUsuarios(ListaUser& lista):
		menu_opcao("Listar Usuarios",lista)
	{};
	int exec();
}; //ListarUsuarios

} //namespace nrc

#endif /* SRC_LISTARUSUARIOS_H_ */
