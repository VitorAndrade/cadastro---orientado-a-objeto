/*
 * ProcurarId.h
 *
 *  Created on: 30 de out de 2019
 *      Author: vitor
 */

#ifndef SRC_PROCURARID_H_
#define SRC_PROCURARID_H_

#include "menu_opcao.h"

namespace nrc {

class ProcurarId: public menu_opcao
{
public:

	ProcurarId(ListaUser& lista):
	menu_opcao("Procurar por ID",lista)
	{};

	int exec();
};

}//namespace nrc



#endif /* SRC_PROCURARID_H_ */
