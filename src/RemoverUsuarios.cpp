/*
 * RemoverUsuarios.cpp
 *
 *  Created on: 29 de out de 2019
 *      Author: vitor
 */

#include "RemoverUsuarios.h"

namespace nrc {

int RemoverUsuarios::exec()
{
	int id = 0;

	for(;;)
	{
		std::cout << "digite a id:\n" << std::endl;
		id = le_numero();
		if(id == -1)
		{
			std::cout << "valor inválido, tentente novamente!\n" << std::endl;
			continue;
		}
		if(!_lista.getById(id))
		{
			std::cout << "id não cadastrada!\n" << std::endl;
			return 0;
		}
		break;
	}
	_lista.remove_id(id);
	return 0;
}

} //namespace nrc
