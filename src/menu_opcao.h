/*
 * menu_opcao.h
 *
 *  Created on: 25 de out de 2019
 *      Author: vitor
 */

#ifndef SRC_MENU_OPCAO_H_
#define SRC_MENU_OPCAO_H_

//#include "Lista.h"
#include "lista_usuario.h"
#include <iostream>

namespace nrc {

class menu_opcao
{

private:

	const char* _nome;

protected:
	ListaUser& _lista;

public:
	menu_opcao(const char* nome,ListaUser& lista):
		_nome(nome),
		_lista(lista)
		{};
	virtual ~menu_opcao(){};
	const char* get_nome()
	{
		return _nome;
	};
	virtual int exec()
	{
		std::cout << "exec";
		return 0;
	};
}; //menu_opcao

} //namespace nrc

#endif /* SRC_MENU_OPCAO_H_ */
