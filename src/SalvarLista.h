/*
 * SalvarLista.h
 *
 *  Created on: 4 de nov de 2019
 *      Author: vitor
 */

#ifndef SRC_SALVARLISTA_H_
#define SRC_SALVARLISTA_H_

#include "menu_opcao.h"
#include "Lista.h"

namespace nrc {

class SalvarLista: public menu_opcao
{
public:
	SalvarLista(ListaUser& lista):
		menu_opcao("Salvar Lista",lista)
	{};

	int exec();
}; //end class

} //namespace nrc





#endif /* SRC_SALVARLISTA_H_ */
