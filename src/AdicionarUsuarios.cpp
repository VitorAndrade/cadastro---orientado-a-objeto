/*
 * AdicionarUsuarios.cpp
 *
 *  Created on: 29 de out de 2019
 *      Author: vitor
 */

#include "AdicionarUsuarios.h"
#include "Menu.h"
#include "auxlib.h"
#include <iostream>

namespace nrc {

int AdicionarUsuarios::exec()
{

			std::cout << "adicionar usuário" << std::endl;
			std::cout << std::endl;
			int id = 0;
			char nome[TAM_NOME];
			char time[TAM_TIME];

			for(;;)
			{
				std::cout << "digite a id:" << std::endl;
				id = le_numero();

				if(id == -1 || _lista.getById(id))
				{
					std::cout << "valor inválido, tente novamente\n" << std::endl;
					continue;
				}


				break;
			}


			for(;;)
			{
				std::cout << "digite o nome:\n" << std::endl;
				if(le_linha(nome,TAM_NOME))
				{
					break;
				}
			}
			for(;;)
			{
				std::cout << "digite o time:\n" << std::endl;
				if(le_linha(time,TAM_TIME))
				{
					break;
				}
				std::cout << "time inválido!\n" << std::endl;

				continue;
			}

			unsigned int id2 = (unsigned int)id;
			_lista.adicionarElementoDados(id2,nome,time);

			return 0;
		}

} //namespace nrc
