/*
 * AdicionarArquivo.h
 *
 *  Created on: 31 de out de 2019
 *      Author: vitor
 */

#ifndef SRC_ADICIONARARQUIVO_H_
#define SRC_ADICIONARARQUIVO_H_

#include "menu_opcao.h"
#include "Lista.h"
#include "auxlib.h"
#include "Menu.h"
#include <vector>

namespace nrc {

class AdicionarArquivo:public menu_opcao
{
public:

	AdicionarArquivo(ListaUser& lista):
	menu_opcao("Adicionar Arquivo",lista)
	{};

	int exec();
};

}// namespace nrc


#endif /* SRC_ADICIONARARQUIVO_H_ */
